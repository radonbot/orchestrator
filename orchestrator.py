import discord
import asyncio
import json
import kubernetes

bot = discord.Client()
kubernetes.config.load_incluster_config()
# kubernetes.config.load_kube_config()

with open("./config/config.json", "r") as f:
    CFG = json.load(f)


class Timer:
    def __init__(self, timeout, callback):
        self._timeout = timeout
        self._callback = callback
        self._task = asyncio.ensure_future(self._job())

    async def _job(self):
        await asyncio.sleep(self._timeout)
        await self._callback()

    def cancel(self):
        self._task.cancel()


class GuildTracker:
    def __init__(self):
        self.label = CFG.get("podLabel")
        self.namespace = CFG.get("namespace")

        self.client = discord.Client()
        self.core_kube_client = kubernetes.client.CoreV1Api()
        self.apps_kube_client = kubernetes.client.AppsV1Api()

        # Initialize discord events
        self.client.event(self.on_ready)
        self.client.event(self.on_guild_join)
        self.client.event(self.on_guild_remove)
        self.client.run(self.client.run(CFG.get("discordToken")))

    async def wait_for_pods(self, n):
        times = 0
        while len(await self.get_running_pods()) < n:
            if times > 10:
                print("Timed out waiting for pods")
                return
            print(f"Waiting for {n} pods")
            await asyncio.sleep(3)
            times += 1

    async def get_running_pods(self):
        return [p for p in self.core_kube_client.list_namespaced_pod(
            self.namespace,
            label_selector=f"name={self.label}").items
            if p.status.phase == "Running"]

    async def get_terminating_pods(self):
        return [p for p in self.core_kube_client.list_namespaced_pod(
            self.namespace,
            label_selector=f"name={self.label}").items
            if p.status.phase == "Terminating"]

    async def on_ready(self):
        print("\nPolling all pods")
        guilds = await self.get_guild_ids()
        print(f"Guilds:\n\t{guilds}")
        pods = await self.get_running_pods()
        pod_guild_id = [p.metadata.labels['guild_id']
                        if ('guild_id' in p.metadata.labels)
                        else 'NO ID' for p in pods]
        pod_names = [p.metadata.name for p in pods]
        print("NAME\t\t\t\tGUILD_ID")
        for i in range(len(pod_names)):
            print(f"{pod_names[i]}\t{pod_guild_id[i]}")

        if len(pods) < len(guilds):
            print("Too many running pods for the ammount of guilds")
            await self.scale_deployment(len(guilds))
            await self.wait_for_pods(len(guilds))
        pods = await self.get_running_pods()
        if len(pods) > len(guilds):
            empty_pods = [p for p in pods
                          if ('guild_id' not in p.metadata.labels)]
            for i in range(len(pods) - len(guilds)):
                await self.delete_pod(empty_pods[i])
            await self.scale_deployment(len(guilds))
        if len(pods) > 0:
            for pod in pods:
                if "guild_id" in pod.metadata.labels:
                    try:
                        del guilds[guilds.index(
                            int(pod.metadata.labels["guild_id"]))]
                    except IndexError as e:
                        print("error", e)
                    except ValueError as e:
                        print("error", e)
                        await self.delete_pod(pod)
                        await self.scale_deployment(
                            len(await self.get_guild_ids()))
            for pod in pods:
                if "guild_id" not in pod.metadata.labels:
                    if len(guilds) > 0:
                        print(f"assigning pod {pod.metadata.name} "
                              f"to {guilds[-1]}")
                        await self.assign_pod(pod, guilds.pop())
        self.timer = Timer(30, self.on_ready)

    async def delete_pod(self, pod):
        print(f"Removing {pod.metadata.name}")
        self.core_kube_client.delete_namespaced_pod(
            pod.metadata.name,
            pod.metadata.namespace
        )

    async def assign_pod(self, pod, guild_id):
        print(f"Assigning {pod.metadata.name} to guild id {guild_id}")
        self.core_kube_client.patch_namespaced_pod(
            pod.metadata.name,
            pod.metadata.namespace,
            {
                "metadata": {"labels": {"guild_id": str(guild_id)}}
            }
        )

    async def get_deployment(self):
        try:
            return self.apps_kube_client.list_namespaced_deployment(
                self.namespace,
                label_selector=f"name={self.label}").items[0]
        except IndexError:
            return

    async def scale_deployment(self, size):
        print(f"Scaling deployment to {size} pods")
        deployment = await self.get_deployment()
        if deployment is None:
            return
        self.apps_kube_client.patch_namespaced_deployment_scale(
            deployment.metadata.name,
            deployment.metadata.namespace,
            {"spec": {"replicas": size}})

    async def get_guild_ids(self):
        return [g.id for g in self.client.guilds]

    async def get_pods(self):
        return self.core_kube_client.list_namespaced_pod(
            self.namespace,
            label_selector=f"name={self.label}").items

    async def get_empty_pod(self):
        while 1:
            pods = await self.get_pods()
            for pod in pods:
                if "guild_id" not in pod.metadata.labels:
                    return pod
            await asyncio.sleep(1)
            print("Waiting for empty pod")

    async def on_guild_join(self, guild):
        print(f"\nBot was added to guild {guild.id}")
        pods = (await self.get_pods())
        existing_pods = [p.metadata.labels["guild_id"]
                         for p in pods if "guild_id" in p.metadata.labels]
        if str(guild.id) in existing_pods:
            return
        deployment_scale = (await self.get_deployment()).spec.replicas
        await self.scale_deployment(deployment_scale + 1)
        pod = await self.get_empty_pod()
        await self.assign_pod(pod, guild.id)

    async def on_guild_remove(self, guild):
        print(f"\nBot was removed from guild {guild.id}")
        pods = await self.get_pods()
        pod = [p for p in pods
               if p.metadata.labels.get("guild_id") == str(guild.id)][0]
        await self.delete_pod(pod)
        await self.scale_deployment(len(pods)-1)


if __name__ == "__main__":
    bot = GuildTracker()
