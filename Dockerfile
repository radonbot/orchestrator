FROM python:buster
RUN mkdir /app
COPY . /app/.
RUN pip3 install -r /app/requirements.txt
ENTRYPOINT ["python", "-u", "/app/orchestrator.py"]